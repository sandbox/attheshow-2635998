
OVERVIEW
------------

The DbLog Exprment module is a forked version of the main DbLog Event Simple
Pager (https://www.drupal.org/sandbox/ToddZebert/1972430) for experimentation
with the issue queue on d.o. There are various issues included in the module
that your instructor will assign for you to fix. You can then submit your
patches to the issue queue. Please be sure if you fix something to submit a
patch and help out your fellow developers! Thanks for being a part of the
wonderful Drupal community.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/attheshow/2635998
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2635998
